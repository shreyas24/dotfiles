Repository of config files (currently git, vim, emacs, and bash) a.k.a. dotfiles

Cloning:

    git clone git@bitbucket.org:shreyas24/dotfiles.git ~/dotfiles
    OR
    git clone https://shreyas24@bitbucket.org/shreyas24/dotfiles.git ~/dotfiles

Installing Emacs Doom

    git clone https://github.com/hlissner/doom-emacs ~/.emacsdoom.d
    ~/.emacsdoom.d/bin/doom install

Create symlinks:

    ln -s ~/dotfiles/vim ~/.vim
    ln -s ~/dotfiles/vimrc ~/.vimrc
    ln -s ~/dotfiles/bashrc ~/.bashrc
    ln -s ~/dotfiles/gitconfig ~/.gitconfig
    ln -s ~/dotfiles/pydistutils.cfg ~/.pydistutils.cfg
    ln -s ~/dotfiles/cvsignore ~/.cvsignore
    ln -s ~/dotfiles/vim/vim.d ~/.local/share/applications/vim.desktop
    ln -s ~/dotfiles/vim/gVim.d ~/.local/share/applications/gVim.desktop
    ln -s ~/dotfiles/emacs.d/* ~/.emacs.d
    ln -s ~/dotfiles/doom.d ~/.doom.d

Switch to the `~/dotfiles` directory, and fetch submodules:

    cd ~/dotfiles
    git submodule init
    git submodule update

If git submodule is not supported:

    bash ~/.vim/clone_all.sh

After submodules are setup, make solarized profiles for gnome-terminal:

    bash ~/dotfiles/vim/gnome-terminal-colors-solarized/install.sh

Entire repo can be downloaded from bitbucket along with submodules as a tar.gz file

### Installing emacs on Mac
Homebrew users have a number of formulas available to them. Before they can be
installed, start with Doom's dependencies:

    brew install coreutils git ripgrep fd clang

For Emacs itself these three are the best options, ordered from most to least
recommended for Doom (based on compatibility).

-   [emacs-plus](https://github.com/d12frosted/homebrew-emacs-plus) (the safest option):

        brew tap d12frosted/emacs-plus
        brew install emacs-plus
        ln -s /usr/local/opt/emacs-plus/Emacs.app /Applications/Emacs.app

-   [emacs](https://formulae.brew.sh/formula/emacs) is another acceptable option.

        brew install emacs

-   [emacs-mac](https://bitbucket.org/mituharu/emacs-mac/overview) is another acceptable option. It offers slightly better integration
    with macOS, native emojis and better childframe support. However, at the time
    of writing, it [lacks multi-tty support](https://github.com/railwaycat/homebrew-emacsmacport/issues/52) (which impacts daemon usage).

        brew tap railwaycat/emacsmacport
        brew install emacs-mac
        ln -s /usr/local/opt/emacs-mac/Emacs.app /Applications/Emacs.app
