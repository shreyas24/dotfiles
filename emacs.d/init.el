;;; init.el --- Where all the magic begins
;;
;; This file loads our Emacs initialization from Emacs lisp embedded
;; in literate Org-mode files.

;;; Put custom-set-variables and custom-set-faces above this comment

(org-mode)
(org-babel-load-file "~/.emacs.d/everything.org")

;;; init.el ends here
