execute pathogen#infect()
Helptags
"" List of repo urls of plugins currently using
" https://github.com/kien/ctrlp.vim.git
" https://github.com/sjl/gundo.vim.git
" https://github.com/adimit/prolog.vim.git
" https://github.com/tpope/vim-abolish.git
" https://github.com/altercation/vim-colors-solarized.git
" https://github.com/tpope/vim-commentary.git
" https://github.com/tpope/vim-fugitive.git
" https://github.com/wlangstroth/vim-racket.git
" https://github.com/tpope/vim-repeat.git
" https://github.com/derekwyatt/vim-scala.git
" https://github.com/tpope/vim-sleuth.git
" https://github.com/tpope/vim-surround.git
" https://github.com/tpope/vim-unimpaired.git



" The usual settings
set nocompatible
set autoindent
set ruler
" set relativenumber
set linebreak
set hidden
syntax enable
set backspace=indent,eol,start
set ff=unix

" Default status bar same as the one when multiple windows are opened
set laststatus=2

if has('mouse')
	set mouse=a
endif

set background=light
colorscheme solarized
set t_Co=16  " For Terminal colors to work properly
set guifont=Monaco:h22

" Searching made easier
set ignorecase
set smartcase
set incsearch

set shell=bash\ --login

" set foldmethod=indent
nnoremap <Space> za

nnoremap <c-s> :w<CR>
inoremap <c-s> <ESC>:w<CR><ESC><ESC>

nnoremap <CR> o<ESC>

if has("autocmd")
	au FileType tex let b:my_compiler = 'pdflatex "%"'
	au FileType sh let b:my_compiler = 'bash "%"'
	au FileType c let b:my_compiler = 'gcc -o %:r.o  "%" && ./%:r.o'
	au FileType cpp let b:my_compiler = 'g++ -o %:r.o  "%"'
	au FileType python let b:my_compiler = 'python "%"'
	au FileType java let b:my_compiler = 'javac -d . "%" && java "%:r"'

	function! My_execute(command)
		execute '!' . a:command 
	endfunction

	" Mapping for easy compilation
	au FileType sh,c,cpp,java nnoremap
		\<buffer> <Leader>m :call My_execute(b:my_compiler)<CR>

	" python 3 and 2 compile separately
	" python 2 is default
	au FileType python nnoremap
		\<buffer> <Leader>m3 :call My_execute('python3 "%"')<CR>
	au FileType python nnoremap
		\<buffer> <Leader>m2 :call My_execute(b:my_compiler)<CR>
	au FileType python nnoremap
		\<buffer> <Leader>mm :call My_execute(b:my_compiler)<CR>

	" Easier compilation for tex
	au FileType tex nnoremap
		\<buffer> <Leader>m :call My_execute(b:my_compiler)<CR><CR>
		\ :!evince %:r.pdf &<CR><CR>
	" au FileType tex nnoremap <buffer> <c-s>
	" 	\ <ESC>:w<CR>:call My_execute(b:my_compiler)<CR><CR>
	" 	\ :!evince %:r.pdf &<CR><CR>
	" au FileType tex inoremap <buffer> <c-s>
	" 	\ <ESC>:w<CR>:call My_execute(b:my_compiler)<CR><CR>
	" 	\ :!evince %:r.pdf &<CR><CR>a

	" Commenting blocks of code.
	au FileType c,cpp,java,scala let b:commentary_format = '// %s'
	au FileType sh,ruby,python   let b:commentary_format = '# %s'
	au FileType conf,fstab       let b:commentary_format = '# %s'
	au FileType scheme,lisp      let b:commentary_format = '; %s'
	au FileType tex              let b:commentary_format = '% %s'
	au FileType vim              let b:commentary_format = '" %s'


	au FileType html,xhtml setl ofu=htmlcomplete#CompleteTags
	au FileType c setl ofu=ccomplete#CompleteCpp
	au FileType css setl ofu=csscomplete#CompleteCSS
	" au Filetype java setl omnifunc=javacomplete#Complete
endif

nnoremap <Leader>wn :call matchadd('ExtraWhitespace', 
			\'^\s* \s*\<Bar>\s\+$')<CR>
nnoremap <Leader>wf :call UndoHighlight('ExtraWhitespace')<CR>
nnoremap <Leader>ss :mksession! ~/vim_sessions/
nnoremap <Leader>rs :e ~/vim_sessions<CR>:source ~/vim_sessions/
nnoremap <Leader>cd :cd %:p:h<CR>
nnoremap <Leader>d :r !date +\%a\ \%d\ \%B\ \%Y<CR>
nnoremap <Leader>b :!gnome-terminal --working-directory="%:p:h" &<CR><CR>
nnoremap <Leader>ww :set invwrap<CR>:call HighlightOverLength()<CR>
nnoremap <Leader>p "+p
nnoremap <Leader>tt :set expandtab ts=4 sw=4<CR>

" Show & highlight leading whitespace that includes spaces,
" and trailing whitespace.
highlight ExtraWhitespace ctermbg=red guibg=red

function! HighlightOverLength()
	if &background =~ 'light'
		highlight OverLength ctermbg=grey guibg=grey
	else
		highlight OverLength ctermbg=black guibg=black
	endif
	if &wrap !~ 0
		call matchadd('OverLength', '\%>80v.')
	else
		call UndoHighlight('OverLength')
	endif
endfun

function! UndoHighlight(groupName)
	for m in getmatches()
		if m.group ==# a:groupName
			call matchdelete(m.id)
		endif
	endfor
endfun

call HighlightOverLength()

" Function key mappings (make a little use of them!)
nnoremap <F5> :GundoToggle<CR>
nnoremap <F7> :silent make \| redraw!<Return>
nnoremap <F8> :cprevious<Return>
nnoremap <F9> :cnext<Return>

inoremap <Left> <Nop>
inoremap <Right> <Nop>
inoremap <Up> <Nop>
inoremap <Down> <Nop>


" Mappings for insert (paste) mode
set pastetoggle=<S-F12>
noremap <S-F12> :set invpaste<CR>

" Abbreviations
iab <expr> \d strftime("%a %d %B %Y")

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

let g:ctrlp_show_hidden = 1

cd %:p:h
