[Desktop Entry]
Name=Vim
Comment=Edit file in Vim
Exec=vim %F
Icon=vim
Terminal=true
Type=Application
Categories=Development;
StartupNotify=true
NoDisplay=true
StartupWMClass=Vim
Name[en_IN]=Vim
Comment[en_IN]=Edit file in Vim
