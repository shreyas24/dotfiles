#!/bin/bash
git clone https://github.com/tpope/vim-fugitive.git ./bundle/vim-fugitive
git clone https://github.com/tpope/vim-sleuth.git ./bundle/vim-sleuth
git clone https://github.com/tpope/vim-surround.git ./bundle/vim-surround
git clone https://github.com/tpope/vim-commentary.git ./bundle/vim-commentary
git clone https://github.com/derekwyatt/vim-scala.git ./bundle/vim-scala
git clone https://github.com/kien/ctrlp.vim.git ./bundle/ctrlp
git clone https://github.com/altercation/vim-colors-solarized.git ./bundle/vim-colors-solarized
git clone https://github.com/Anthony25/gnome-terminal-colors-solarized.git ./gnome-terminal-colors-solarized
git clone https://github.com/tpope/vim-unimpaired.git ./bundle/vim-unimpaired
git clone https://github.com/tpope/vim-abolish.git ./bundle/vim-abolish
git clone https://github.com/wlangstroth/vim-racket.git ./bundle/vim-racket
