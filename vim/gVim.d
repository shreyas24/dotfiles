[Desktop Entry]
Name=gVim
Comment=Edit file in gVim
Exec=gvim -f %F
Icon=gvim
Terminal=false
Type=Application
Categories=Development;
StartupNotify=true
NoDisplay=true
StartupWMClass=Vim
Name[en_IN]=gVim
Comment[en_IN]=Edit file in gVim
